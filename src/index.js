import express from 'express';
import cors from 'cors';

const app = express();
app.use(cors());

app.get('/', (req, res) => {
  res.json({
    hello: 'JS World',
  });
});

app.get('/task2a', (req, res) => {
  const summ = (+req.query.a || 0) + (+req.query.b || 0);
  res.send(summ.toString());
});

app.get('/task2b', (req, res) => {
	const fullname = req.query.fullname;
	if(fullname) {
		console.log('fullname', fullname);
		let arrName;
		if(/[0-9_\/*]/.test(fullname)){
			arrName = [];
		} else {
			arrName = fullname.split(' ').filter(function(item){return item.length});
		}
		switch(arrName.length) {
			case 3:
				res.send(`${arrName[2][0].toUpperCase() + arrName[2].slice(1).toLowerCase()} ${arrName[0][0].toUpperCase()}. ${arrName[1][0].toUpperCase()}.`);
				break;
			case 2:
				res.send(`${arrName[1][0].toUpperCase() + arrName[1].slice(1).toLowerCase()} ${arrName[0][0].toUpperCase()}.`);
				break;
			case 1:
				res.send(`${arrName[0][0].toUpperCase() + arrName[0].slice(1).toLowerCase()}`);
			default:
				res.send('Invalid fullname');
		}
	} else {
		res.send('Invalid fullname');
	}
});

app.get('/task2c', (req, res) => {
	const fullname = req.query.username;
	let protocol = fullname.split('//'),
		name,
		result;

	if(protocol[1]) {
		name = protocol[1].split('/');
		result = name[1][0] === '@' ? `${name[1]}` : `@${name[1]}`;
	} else {
		name = protocol[0].split('/');
		if(name.length > 1) {	
			result = name[1][0] === '@' ? `${name[1]}` : `@${name[1]}`;
		} else {
			result = name[0][0] === '@' ? `${name[0]}` : `@${name[0]}`;;
		}
	}

	res.send(result);
});

app.listen(3000, () => {
  console.log('Your app listening on port 3000!');
});
